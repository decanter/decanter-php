# Swagger\Client\EnclosureApi

All URIs are relative to *https://localhost/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**enclosureCreate**](EnclosureApi.md#enclosureCreate) | **POST** /enclosures | Create a new enclosure
[**enclosureDelete**](EnclosureApi.md#enclosureDelete) | **DELETE** /enclosures/{enclosure_id} | delete an enclosure envelope
[**enclosureGet**](EnclosureApi.md#enclosureGet) | **GET** /enclosures/{enclosure_id} | get an enclosure envelope
[**enclosureList**](EnclosureApi.md#enclosureList) | **GET** /enclosures | Get a list of enclosures
[**enclosureStream**](EnclosureApi.md#enclosureStream) | **GET** /enclosures/{enclosure_id}/{stream}.{ext} | Fetch an episode stream


# **enclosureCreate**
> \Swagger\Client\Models\Enclosure enclosureCreate($name, $description, $file)

Create a new enclosure

Creates a new enclosure object

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\EnclosureApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$name = "name_example"; // string | The enclosure name
$description = "description_example"; // string | The enclosure description
$file = "/path/to/file.txt"; // \SplFileObject | The enclosure data

try {
    $result = $apiInstance->enclosureCreate($name, $description, $file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnclosureApi->enclosureCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string**| The enclosure name |
 **description** | **string**| The enclosure description | [optional]
 **file** | **\SplFileObject**| The enclosure data | [optional]

### Return type

[**\Swagger\Client\Models\Enclosure**](../Model/Enclosure.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **enclosureDelete**
> enclosureDelete($enclosure_id)

delete an enclosure envelope

deletes the enclosure and associated objects

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\EnclosureApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$enclosure_id = "enclosure_id_example"; // string | The enclosure to get

try {
    $apiInstance->enclosureDelete($enclosure_id);
} catch (Exception $e) {
    echo 'Exception when calling EnclosureApi->enclosureDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enclosure_id** | [**string**](../Model/.md)| The enclosure to get |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **enclosureGet**
> \Swagger\Client\Models\Enclosure enclosureGet($enclosure_id)

get an enclosure envelope

gets the meta data for the enclosure

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\EnclosureApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$enclosure_id = "enclosure_id_example"; // string | The enclosure to get

try {
    $result = $apiInstance->enclosureGet($enclosure_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnclosureApi->enclosureGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enclosure_id** | [**string**](../Model/.md)| The enclosure to get |

### Return type

[**\Swagger\Client\Models\Enclosure**](../Model/Enclosure.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **enclosureList**
> \Swagger\Client\Models\Enclosure[] enclosureList($preload_episodes)

Get a list of enclosures

Returns an array of enclosures

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\EnclosureApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$preload_episodes = false; // bool | if true the episode list for the enclosure is returned

try {
    $result = $apiInstance->enclosureList($preload_episodes);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnclosureApi->enclosureList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **preload_episodes** | **bool**| if true the episode list for the enclosure is returned | [optional] [default to false]

### Return type

[**\Swagger\Client\Models\Enclosure[]**](../Model/Enclosure.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **enclosureStream**
> string enclosureStream($enclosure_id, $ext, $stream)

Fetch an episode stream

Returns the enclosure stream

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\EnclosureApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$enclosure_id = "enclosure_id_example"; // string | id of the enclosure
$ext = "ext_example"; // string | The enclosure extension
$stream = "stream_example"; // string | 

try {
    $result = $apiInstance->enclosureStream($enclosure_id, $ext, $stream);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnclosureApi->enclosureStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enclosure_id** | [**string**](../Model/.md)| id of the enclosure |
 **ext** | **string**| The enclosure extension |
 **stream** | **string**|  |

### Return type

**string**

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: audio/mpeg

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

