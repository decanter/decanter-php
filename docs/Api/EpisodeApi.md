# Swagger\Client\EpisodeApi

All URIs are relative to *https://localhost/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**episodeCreate**](EpisodeApi.md#episodeCreate) | **POST** /episodes | Create a new episode in a series
[**episodeGet**](EpisodeApi.md#episodeGet) | **GET** /episodes/{episode_id} | Fetch an episode object
[**episodeList**](EpisodeApi.md#episodeList) | **GET** /episodes | Get a list of episodes
[**episodeStream**](EpisodeApi.md#episodeStream) | **GET** /episodes/{episode_id}/{stream}.{ext} | Fetch an episode stream
[**episodeUpdate**](EpisodeApi.md#episodeUpdate) | **PUT** /episodes/{episode_id} | Update an episode


# **episodeCreate**
> \Swagger\Client\Models\Episode episodeCreate($episode)

Create a new episode in a series

Creates a new episode in the database

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\EpisodeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$episode = new \Swagger\Client\Models\Episode(); // \Swagger\Client\Models\Episode | The episode to create

try {
    $result = $apiInstance->episodeCreate($episode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EpisodeApi->episodeCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **episode** | [**\Swagger\Client\Models\Episode**](../Model/Episode.md)| The episode to create | [optional]

### Return type

[**\Swagger\Client\Models\Episode**](../Model/Episode.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **episodeGet**
> \Swagger\Client\Models\Episode episodeGet($episode_id)

Fetch an episode object

Returns the episode object

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\EpisodeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$episode_id = "episode_id_example"; // string | id of the episode

try {
    $result = $apiInstance->episodeGet($episode_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EpisodeApi->episodeGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **episode_id** | [**string**](../Model/.md)| id of the episode |

### Return type

[**\Swagger\Client\Models\Episode**](../Model/Episode.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **episodeList**
> \Swagger\Client\Models\Episode[] episodeList($series_id, $unpublished)

Get a list of episodes

Returns a list of episodes

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\EpisodeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$series_id = array("series_id_example"); // string[] | The series episodes to return
$unpublished = false; // bool | return unplublished episodes only

try {
    $result = $apiInstance->episodeList($series_id, $unpublished);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EpisodeApi->episodeList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **series_id** | [**string[]**](../Model/string.md)| The series episodes to return | [optional]
 **unpublished** | **bool**| return unplublished episodes only | [optional] [default to false]

### Return type

[**\Swagger\Client\Models\Episode[]**](../Model/Episode.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **episodeStream**
> string episodeStream($episode_id, $stream, $ext)

Fetch an episode stream

Returns the episode stream

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\EpisodeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$episode_id = "episode_id_example"; // string | id of the episode
$stream = "stream_example"; // string | 
$ext = "ext_example"; // string | The enclosure extension

try {
    $result = $apiInstance->episodeStream($episode_id, $stream, $ext);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EpisodeApi->episodeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **episode_id** | [**string**](../Model/.md)| id of the episode |
 **stream** | **string**|  |
 **ext** | **string**| The enclosure extension |

### Return type

**string**

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: audio/mpeg

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **episodeUpdate**
> \Swagger\Client\Models\Episode episodeUpdate($episode_id, $episode)

Update an episode

Updates the episode object

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\EpisodeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$episode_id = "episode_id_example"; // string | id of the episode
$episode = new \Swagger\Client\Models\Episode(); // \Swagger\Client\Models\Episode | The episode to update

try {
    $result = $apiInstance->episodeUpdate($episode_id, $episode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EpisodeApi->episodeUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **episode_id** | [**string**](../Model/.md)| id of the episode |
 **episode** | [**\Swagger\Client\Models\Episode**](../Model/Episode.md)| The episode to update | [optional]

### Return type

[**\Swagger\Client\Models\Episode**](../Model/Episode.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

