# Swagger\Client\MemberApi

All URIs are relative to *https://localhost/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**memberAccessLogList**](MemberApi.md#memberAccessLogList) | **GET** /members/{member_id}/logs | Gets a member access log
[**memberFeedCreate**](MemberApi.md#memberFeedCreate) | **POST** /members/{member_id}/feed | Initialize the members feed
[**memberFeedGet**](MemberApi.md#memberFeedGet) | **GET** /members/{member_id}/feed | Query the rss feed for the member
[**memberFeedUpdate**](MemberApi.md#memberFeedUpdate) | **PUT** /members/{member_id}/feed | Update the members feed
[**memberTokenCreate**](MemberApi.md#memberTokenCreate) | **POST** /members/{member_id}/tokens | Generate a new member token
[**memberTokenList**](MemberApi.md#memberTokenList) | **GET** /members/{member_id}/tokens | Gets a members tokens
[**memberTokenRevoke**](MemberApi.md#memberTokenRevoke) | **DELETE** /members/{member_id}/tokens | Revoke a member token


# **memberAccessLogList**
> \Swagger\Client\Models\LogEntry[] memberAccessLogList($member_id, $member_token_id, $series_id, $limit, $offset, $start, $end, $order, $order_dir)

Gets a member access log

Returns log enties for the member

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\MemberApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$member_id = "member_id_example"; // string | The member id
$member_token_id = array("member_token_id_example"); // string[] | member tokens to get logs for
$series_id = "series_id_example"; // string | The series id to get logs for
$limit = 100; // int | Limit the number of items retured
$offset = 0; // int | The offset
$start = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$end = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$order = "time"; // string | 
$order_dir = "desc"; // string | 

try {
    $result = $apiInstance->memberAccessLogList($member_id, $member_token_id, $series_id, $limit, $offset, $start, $end, $order, $order_dir);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MemberApi->memberAccessLogList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **member_id** | **string**| The member id |
 **member_token_id** | [**string[]**](../Model/string.md)| member tokens to get logs for | [optional]
 **series_id** | [**string**](../Model/.md)| The series id to get logs for | [optional]
 **limit** | **int**| Limit the number of items retured | [optional] [default to 100]
 **offset** | **int**| The offset | [optional] [default to 0]
 **start** | **\DateTime**|  | [optional]
 **end** | **\DateTime**|  | [optional]
 **order** | **string**|  | [optional] [default to time]
 **order_dir** | **string**|  | [optional] [default to desc]

### Return type

[**\Swagger\Client\Models\LogEntry[]**](../Model/LogEntry.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **memberFeedCreate**
> \Swagger\Client\Models\Feed memberFeedCreate($member_id, $feed)

Initialize the members feed

Initializes a member feed if it doesnt exist

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\MemberApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$member_id = "member_id_example"; // string | id of the member
$feed = new \Swagger\Client\Models\Feed(); // \Swagger\Client\Models\Feed | The feed

try {
    $result = $apiInstance->memberFeedCreate($member_id, $feed);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MemberApi->memberFeedCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **member_id** | **string**| id of the member |
 **feed** | [**\Swagger\Client\Models\Feed**](../Model/Feed.md)| The feed | [optional]

### Return type

[**\Swagger\Client\Models\Feed**](../Model/Feed.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **memberFeedGet**
> string memberFeedGet($member_id, $series_id, $max_length, $start_date, $end_date)

Query the rss feed for the member

Gets a member feed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\MemberApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$member_id = "member_id_example"; // string | id of the member
$series_id = "series_id_example"; // string | The series for the feed
$max_length = 789; // int | The number of episodes to return
$start_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | The start date for the query
$end_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | The end date for the query

try {
    $result = $apiInstance->memberFeedGet($member_id, $series_id, $max_length, $start_date, $end_date);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MemberApi->memberFeedGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **member_id** | **string**| id of the member |
 **series_id** | [**string**](../Model/.md)| The series for the feed | [optional]
 **max_length** | **int**| The number of episodes to return | [optional]
 **start_date** | **\DateTime**| The start date for the query | [optional]
 **end_date** | **\DateTime**| The end date for the query | [optional]

### Return type

**string**

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **memberFeedUpdate**
> \Swagger\Client\Models\Feed memberFeedUpdate($member_id, $feed)

Update the members feed

Updates a member feed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\MemberApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$member_id = "member_id_example"; // string | id of the member
$feed = new \Swagger\Client\Models\Feed(); // \Swagger\Client\Models\Feed | The feed

try {
    $result = $apiInstance->memberFeedUpdate($member_id, $feed);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MemberApi->memberFeedUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **member_id** | **string**| id of the member |
 **feed** | [**\Swagger\Client\Models\Feed**](../Model/Feed.md)| The feed | [optional]

### Return type

[**\Swagger\Client\Models\Feed**](../Model/Feed.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **memberTokenCreate**
> \Swagger\Client\Models\BearerToken memberTokenCreate($member_id, $expires_at, $revoke_existing)

Generate a new member token

Generates a new bearer token from the

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\MemberApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$member_id = "member_id_example"; // string | The member id
$expires_at = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$revoke_existing = false; // bool | 

try {
    $result = $apiInstance->memberTokenCreate($member_id, $expires_at, $revoke_existing);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MemberApi->memberTokenCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **member_id** | **string**| The member id |
 **expires_at** | **\DateTime**|  | [optional]
 **revoke_existing** | **bool**|  | [optional] [default to false]

### Return type

[**\Swagger\Client\Models\BearerToken**](../Model/BearerToken.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **memberTokenList**
> \Swagger\Client\Models\MemberToken[] memberTokenList($member_id, $limit, $after_id, $sort, $include_revoked)

Gets a members tokens

Returns the token for this member

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\MemberApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$member_id = "member_id_example"; // string | The member id
$limit = 56; // int | Limit the number of items retured
$after_id = "after_id_example"; // string | Return the tokens with id after this one
$sort = array("sort_example"); // string[] | Sort on fields
$include_revoked = false; // bool | 

try {
    $result = $apiInstance->memberTokenList($member_id, $limit, $after_id, $sort, $include_revoked);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MemberApi->memberTokenList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **member_id** | **string**| The member id |
 **limit** | **int**| Limit the number of items retured | [optional]
 **after_id** | [**string**](../Model/.md)| Return the tokens with id after this one | [optional]
 **sort** | [**string[]**](../Model/string.md)| Sort on fields | [optional]
 **include_revoked** | **bool**|  | [optional] [default to false]

### Return type

[**\Swagger\Client\Models\MemberToken[]**](../Model/MemberToken.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **memberTokenRevoke**
> memberTokenRevoke($member_id, $token_id, $delete)

Revoke a member token

Deletes a member token

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\MemberApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$member_id = "member_id_example"; // string | The member id
$token_id = array("token_id_example"); // string[] | 
$delete = false; // bool | 

try {
    $apiInstance->memberTokenRevoke($member_id, $token_id, $delete);
} catch (Exception $e) {
    echo 'Exception when calling MemberApi->memberTokenRevoke: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **member_id** | **string**| The member id |
 **token_id** | [**string[]**](../Model/string.md)|  | [optional]
 **delete** | **bool**|  | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

