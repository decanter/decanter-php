# Swagger\Client\OauthApi

All URIs are relative to *https://localhost/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**oauthTokenGet**](OauthApi.md#oauthTokenGet) | **POST** /oauth/token | Get a new auth token


# **oauthTokenGet**
> \Swagger\Client\Models\BearerToken oauthTokenGet($grant_type, $client_id, $scope)

Get a new auth token

Returns a new oauth 2.0 token for use with the Decanter API

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: clientSecret
$config = Swagger\Client\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Swagger\Client\Api\OauthApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$grant_type = "grant_type_example"; // string | The authorization grant type
$client_id = "client_id_example"; // string | The client id
$scope = array("scope_example"); // string[] | the scopes

try {
    $result = $apiInstance->oauthTokenGet($grant_type, $client_id, $scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OauthApi->oauthTokenGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **grant_type** | **string**| The authorization grant type |
 **client_id** | **string**| The client id | [optional]
 **scope** | [**string[]**](../Model/string.md)| the scopes | [optional]

### Return type

[**\Swagger\Client\Models\BearerToken**](../Model/BearerToken.md)

### Authorization

[clientSecret](../../README.md#clientSecret)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

