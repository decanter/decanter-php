# Swagger\Client\SeriesApi

All URIs are relative to *https://localhost/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**memberStatsGet**](SeriesApi.md#memberStatsGet) | **GET** /series/{series_id}/member-stats | Gets a members stats
[**seriesCreate**](SeriesApi.md#seriesCreate) | **POST** /series | Create a new series
[**seriesDelete**](SeriesApi.md#seriesDelete) | **DELETE** /series/{series_id} | Delete a series
[**seriesFeedGet**](SeriesApi.md#seriesFeedGet) | **GET** /series/{series_id}/feed | Returns a feed for a series
[**seriesGet**](SeriesApi.md#seriesGet) | **GET** /series/{series_id} | Get a series
[**seriesList**](SeriesApi.md#seriesList) | **GET** /series | Get all series
[**seriesUpdate**](SeriesApi.md#seriesUpdate) | **PUT** /series/{series_id} | Update a series


# **memberStatsGet**
> \Swagger\Client\Models\MemberStats[] memberStatsGet($series_id, $member_id, $offset, $limit, $start, $end, $order, $order_dir, $revoked)

Gets a members stats

Returns the stats for this member

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\SeriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$series_id = "series_id_example"; // string | id of the series
$member_id = array("member_id_example"); // string[] | The member id
$offset = 0; // int | 
$limit = 100; // int | 
$start = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$end = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$order = "downloads"; // string | 
$order_dir = "desc"; // string | 
$revoked = false; // bool | 

try {
    $result = $apiInstance->memberStatsGet($series_id, $member_id, $offset, $limit, $start, $end, $order, $order_dir, $revoked);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SeriesApi->memberStatsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **series_id** | [**string**](../Model/.md)| id of the series |
 **member_id** | [**string[]**](../Model/string.md)| The member id | [optional]
 **offset** | **int**|  | [optional] [default to 0]
 **limit** | **int**|  | [optional] [default to 100]
 **start** | **\DateTime**|  | [optional]
 **end** | **\DateTime**|  | [optional]
 **order** | **string**|  | [optional] [default to downloads]
 **order_dir** | **string**|  | [optional] [default to desc]
 **revoked** | **bool**|  | [optional] [default to false]

### Return type

[**\Swagger\Client\Models\MemberStats[]**](../Model/MemberStats.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **seriesCreate**
> \Swagger\Client\Models\Series seriesCreate($series)

Create a new series

Creates a new series in the database

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\SeriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$series = new \Swagger\Client\Models\Series(); // \Swagger\Client\Models\Series | The podcast to create a series for

try {
    $result = $apiInstance->seriesCreate($series);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SeriesApi->seriesCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **series** | [**\Swagger\Client\Models\Series**](../Model/Series.md)| The podcast to create a series for | [optional]

### Return type

[**\Swagger\Client\Models\Series**](../Model/Series.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **seriesDelete**
> seriesDelete($series_id)

Delete a series

Removes a series from the database

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\SeriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$series_id = "series_id_example"; // string | id of the series

try {
    $apiInstance->seriesDelete($series_id);
} catch (Exception $e) {
    echo 'Exception when calling SeriesApi->seriesDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **series_id** | [**string**](../Model/.md)| id of the series |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **seriesFeedGet**
> seriesFeedGet($series_id)

Returns a feed for a series

Returns a feed for the specific series

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\SeriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$series_id = "series_id_example"; // string | id of the series

try {
    $apiInstance->seriesFeedGet($series_id);
} catch (Exception $e) {
    echo 'Exception when calling SeriesApi->seriesFeedGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **series_id** | [**string**](../Model/.md)| id of the series |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **seriesGet**
> \Swagger\Client\Models\Series seriesGet($series_id)

Get a series

Get a series from the database

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\SeriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$series_id = "series_id_example"; // string | id of the series

try {
    $result = $apiInstance->seriesGet($series_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SeriesApi->seriesGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **series_id** | [**string**](../Model/.md)| id of the series |

### Return type

[**\Swagger\Client\Models\Series**](../Model/Series.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **seriesList**
> \Swagger\Client\Models\Series[] seriesList()

Get all series

Returns an array of all series from the database

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\SeriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->seriesList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SeriesApi->seriesList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Models\Series[]**](../Model/Series.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **seriesUpdate**
> \Swagger\Client\Models\Series seriesUpdate($series_id, $series)

Update a series

Updates a series in the database

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\SeriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$series_id = "series_id_example"; // string | id of the series
$series = new \Swagger\Client\Models\Series(); // \Swagger\Client\Models\Series | The podcast to create a series for

try {
    $result = $apiInstance->seriesUpdate($series_id, $series);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SeriesApi->seriesUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **series_id** | [**string**](../Model/.md)| id of the series |
 **series** | [**\Swagger\Client\Models\Series**](../Model/Series.md)| The podcast to create a series for | [optional]

### Return type

[**\Swagger\Client\Models\Series**](../Model/Series.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

