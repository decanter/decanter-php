# Swagger\Client\WebhookApi

All URIs are relative to *https://localhost/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**eventCallbackGenerate**](WebhookApi.md#eventCallbackGenerate) | **GET** /event | Get the event callback
[**eventPost**](WebhookApi.md#eventPost) | **POST** /event | Post from a webhook


# **eventCallbackGenerate**
> string eventCallbackGenerate($regenerate)

Get the event callback

Get a callback or regenerate a new one

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\WebhookApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$regenerate = false; // bool | Create a new callback

try {
    $result = $apiInstance->eventCallbackGenerate($regenerate);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhookApi->eventCallbackGenerate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **regenerate** | **bool**| Create a new callback | [optional] [default to false]

### Return type

**string**

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **eventPost**
> eventPost($event)

Post from a webhook

External webhooks post json payloads to this path

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\WebhookApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$event = "B"; // string | The raw event object

try {
    $apiInstance->eventPost($event);
} catch (Exception $e) {
    echo 'Exception when calling WebhookApi->eventPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event** | **string**| The raw event object | [optional]

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

