# BearerToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **string** | The token to be used for authorization | [optional] 
**token_type** | **string** | The token type | [optional] 
**expires_in** | **int** | The time from &#x60;now&#x60; that the token expires | [optional] 
**resource** | **string** | The valid resource identifier for the token | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


