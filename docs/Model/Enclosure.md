# Enclosure

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | The series id | [optional] 
**prefix** | **string** | The enclosure storage prefix | [optional] 
**name** | **string** | The enclosure name | [optional] 
**description** | **string** | The enclosure description | [optional] 
**url** | **string** | The enclosure url | [optional] 
**length** | **int** | The enclosure size in bytes | [optional] 
**duration** | **string** | The enclosure duration | [optional] 
**uploaded_at** | [**\DateTime**](\DateTime.md) | The enclosure upload date | [optional] 
**type** | **string** | The enclosure type | [optional] 
**episodes** | [**\Swagger\Client\Models\Episode[]**](Episode.md) | The episode the enclosure is associated with | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


