# Episode

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | The series id | [optional] 
**series_id** | **string** | The episode series id | [optional] 
**enclosure_id** | **string** | The enclosure id | [optional] 
**enclosure** | [**\Swagger\Client\Models\Enclosure**](Enclosure.md) |  | [optional] 
**allow_any_user** | **bool** | Any valid member can access the feed | [optional] 
**allow_any_subscriber** | **bool** | Any valid member with a subscription can access the feed | [optional] 
**allow_plans** | [**\Swagger\Client\Models\StringArray**](StringArray.md) |  | [optional] 
**guid** | **string** | The episode guid | [optional] 
**title** | **string** | The episode title | [optional] 
**link** | **string** | The episode link | [optional] 
**description** | **string** | The episode description | [optional] 
**content** | **string** | The content:encoded value for the episode | [optional] 
**author** | [**\Swagger\Client\Models\Author**](Author.md) |  | [optional] 
**category** | **string** | The episode category | [optional] 
**comments** | **string** | The episode comments | [optional] 
**source** | **string** | The episosde source | [optional] 
**pub_date** | **string** | The publication date | [optional] 
**itunes_author** | **string** | The itunes author | [optional] 
**itunes_subtitle** | **string** | The itunes subtitle | [optional] 
**itunes_summary** | [**\Swagger\Client\Models\ISummary**](ISummary.md) |  | [optional] 
**itunes_block** | **string** | The itunes block flag | [optional] 
**itunes_image** | [**\Swagger\Client\Models\IImage**](IImage.md) |  | [optional] 
**itunes_duration** | **object** | The itunes duration | [optional] 
**itunes_explicit** | **string** | The itunes explicit flag | [optional] 
**itunes_cc** | **string** | The itunes closed caption flag | [optional] 
**itunes_order** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


