# Feed

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **string** | The title for the feed | [optional] 
**description** | **string** | The description for the feed | [optional] 
**author** | [**\Swagger\Client\Models\Author**](Author.md) |  | [optional] 
**image** | **string** | The image url for the feed | [optional] 
**channel_series_id** | **string** | The series id to use for the channel | [optional] 
**series** | **string[]** | The array of series ids the feed includes if empty will include all series | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


