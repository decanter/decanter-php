# ICategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**text** | **string** |  | [optional] 
**categories** | [**\Swagger\Client\Models\ICategory[]**](ICategory.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


