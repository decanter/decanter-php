# Image

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **string** | The image url | [optional] 
**title** | **string** | The image title | [optional] 
**link** | **string** | The image link | [optional] 
**description** | **string** | The image description | [optional] 
**width** | **int** | The image width | [optional] 
**height** | **int** | The image height | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


