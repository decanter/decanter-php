# LogEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**time** | [**\DateTime**](\DateTime.md) | The time of access | [optional] 
**remote_addr** | **string** | The remote address | [optional] 
**user_agent** | **string** | The user agent | [optional] 
**session_id** | **string** | The session id | [optional] 
**episode_id** | **string** | The episide id | [optional] 
**episode** | [**\Swagger\Client\Models\Episode**](Episode.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


