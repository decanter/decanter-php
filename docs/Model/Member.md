# Member

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | The member id from the provider | [optional] 
**provider** | **string** | The member provider | [optional] 
**username** | **string** | The member username | [optional] 
**name** | **string** | The member full name | [optional] 
**email** | **string** | The member email address | [optional] 
**phone_number** | **string** | The member phone number | [optional] 
**subscriptions** | [**\Swagger\Client\Models\Subscription[]**](Subscription.md) | The member subscriptions | [optional] 
**attributes** | [**\Swagger\Client\Models\StringMap**](StringMap.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


