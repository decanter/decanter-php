# MemberStats

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | [**\DateTime**](\DateTime.md) | The start time for the stat window | [optional] 
**end** | [**\DateTime**](\DateTime.md) | The end time for the stat window | [optional] 
**episodes** | **float** |  | [optional] 
**clients** | **float** |  | [optional] 
**hosts** | **float** |  | [optional] 
**downloads** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


