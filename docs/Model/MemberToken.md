# MemberToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**member_id** | **string** | The member id | [optional] 
**member** | [**\Swagger\Client\Models\Member**](Member.md) |  | [optional] 
**bearer_token** | [**\Swagger\Client\Models\BearerToken**](BearerToken.md) |  | [optional] 
**issued_at** | [**\DateTime**](\DateTime.md) | The issue time | [optional] 
**expires_at** | [**\DateTime**](\DateTime.md) | The expiration | [optional] 
**revoked_at** | [**\DateTime**](\DateTime.md) | The date revoked | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


