# Plan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | The plan id | [optional] 
**provider** | **string** | The plan provider | [optional] 
**name** | **string** | The plan name | [optional] 
**active** | **bool** | The plan status | [optional] 
**price** | **float** | The plan price in cents | [optional] 
**billing_cycle** | **string** | The billing cycle | [optional] 
**billing_interval** | **int** | The multiplier of the cycle | [optional] 
**attributes** | [**\Swagger\Client\Models\StringMap**](StringMap.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


