# Series

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | The series id | [optional] 
**title** | **string** | The series title | [optional] 
**link** | **string** | The series link | [optional] 
**description** | **string** | The series description | [optional] 
**content** | **string** | The series content field | [optional] 
**category** | **string** | Comma separated series categories | [optional] 
**copyright** | **string** | The series copyright message | [optional] 
**rating** | **string** | The series rating | [optional] 
**language** | **string** | The series language | [optional] [default to 'en-us']
**last_build_date** | **string** | The last build date | [optional] 
**author** | [**\Swagger\Client\Models\Author**](Author.md) |  | [optional] 
**web_master** | **string** | The web master | [optional] 
**managing_editor** | **string** | The series author | [optional] 
**pub_date** | **string** | The series publication date | [optional] 
**image** | [**\Swagger\Client\Models\Image**](Image.md) |  | [optional] 
**itunes_author** | **string** | The itunes author | [optional] 
**itunes_subtitle** | **string** | The itunes subtitle | [optional] 
**itunes_summary** | [**\Swagger\Client\Models\ISummary**](ISummary.md) |  | [optional] 
**itunes_block** | **string** | The itunes block flag | [optional] 
**itunes_image** | [**\Swagger\Client\Models\IImage**](IImage.md) |  | [optional] 
**itunes_duration** | **string** | The itunes duration | [optional] 
**itunes_explicit** | **string** | The itunes explicit flag | [optional] 
**itunes_complete** | **string** |  | [optional] 
**itunes_news_feed_url** | **string** |  | [optional] 
**itunes_owner** | [**\Swagger\Client\Models\Author**](Author.md) |  | [optional] 
**itunes_categories** | [**\Swagger\Client\Models\ICategory[]**](ICategory.md) |  | [optional] 
**expired_episode_id** | **string** | The episode to use for expired tokens | [optional] 
**expired_episode** | [**\Swagger\Client\Models\Episode**](Episode.md) |  | [optional] 
**revoked_episode_id** | **string** | The episode to use for revoked tokens | [optional] 
**revoked_episode** | [**\Swagger\Client\Models\Episode**](Episode.md) |  | [optional] 
**is_public** | **bool** | The series feed is public | [optional] 
**allow_any_user** | **bool** | Any valid member can access the feed | [optional] 
**allow_any_subscriber** | **bool** | Any valid member with a subscription can access the feed | [optional] 
**allow_plans** | [**\Swagger\Client\Models\StringArray**](StringArray.md) |  | [optional] 
**is_default** | **bool** | is the default series | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


