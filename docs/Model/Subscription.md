# Subscription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | The subscription id | [optional] 
**provider** | **string** | The subscription provider | [optional] 
**plan_id** | **string** | The plan id | [optional] 
**active** | **bool** | The subscription state | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | The subscription creation time | [optional] 
**expires_at** | [**\DateTime**](\DateTime.md) | The subscription expiration time | [optional] 
**auto_renew** | **bool** | The subscription renewal state | [optional] 
**attributes** | [**\Swagger\Client\Models\StringMap**](StringMap.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


